package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CredentialsValidationRequestDTO implements Serializable {

    private String username;
    private String password;
    private String serial;
    private String userRequesting;

}