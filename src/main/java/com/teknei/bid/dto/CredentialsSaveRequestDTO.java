package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CredentialsSaveRequestDTO implements Serializable {

    private String username;
    private String password;
    private String serial;
    private String userRequesting;
    private String name;
    private String surnameFirst;
    private String surnameLast;
    private Integer noFingers;


}