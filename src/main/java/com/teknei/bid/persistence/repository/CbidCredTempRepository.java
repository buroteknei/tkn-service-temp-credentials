package com.teknei.bid.persistence.repository;

import com.teknei.bid.persistence.entities.CbidCredTemp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CbidCredTempRepository extends JpaRepository<CbidCredTemp, Long> {

    List<CbidCredTemp> findAllByUsuaAndIdDispAndIdEstaCredAndIdEsta(String usua, Long idDisp, Long idEstaCred, Integer idEsta);

    List<CbidCredTemp> findAllByUsuaAndIdDisp(String usua, Long idDisp);

    List<CbidCredTemp> findAllByUsuaAndNoDactAndIdDispAndIdEstaCredAndIdEsta(String usua, Integer noDact, Long idDisp, Long idEstaCred, Integer idEsta);

    List<CbidCredTemp> findAllByIdEstaCred(Long idEstaCred);

}